// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/StaticMeshActor.h"
#include "MyStaticMeshActor_test.generated.h"

/**
 * 
 */
UCLASS()
class PARADOX_API AMyStaticMeshActor_test : public AStaticMeshActor
{
	GENERATED_BODY()
public:
	AMyStaticMeshActor_test();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBlueprint *m_player;
	
};
